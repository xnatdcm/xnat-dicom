#!/bin/bash

for REPO in dicom-image-utils dicom-xnat ecat ecat4xnat extattr prearc-importer session-builders; do
    [[ -d ../${REPO} ]] && { pushd ../${REPO}; git pull; popd; } || { git clone git@bitbucket.org:xnatdcm/${REPO}.git ../${REPO}; }
done

