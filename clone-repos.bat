@echo off

git clone https://bitbucket.org/xnatdcm/dicom-image-utils.git ../dicom-image-utils
git clone https://bitbucket.org/xnatdcm/dicom-xnat.git ../dicom-xnat
git clone https://bitbucket.org/xnatdcm/ecat.git ../ecat
git clone https://bitbucket.org/xnatdcm/ecat4xnat.git ../ecat4xnat
git clone https://bitbucket.org/xnatdcm/extattr.git ../extattr
git clone https://bitbucket.org/xnatdcm/prearc-importer.git ../prearc-importer
git clone https://bitbucket.org/xnatdcm/session-builders.git ../session-builders

