XNAT DICOM Management Libraries
===============================

The XNAT DICOM Management Libraries project is an aggregator project 
that provides a convenient way to build all of the component libraries
used by XNAT and XNAT-related tools for managing DICOM data. It 
includes a number of different libraries as modules, allowing you to
perform bulk builds on the libraries.

Getting the Code
----------------

In order for the aggregate build to work properly, first clone the
**xnat-dicom** repository locally:

```bash
git clone git@bitbucket.org:xnatdcm/xnat-dicom.git
```

The **pom.xml** build file for this repository contains references to
the various repositories that compose the XNAT DICOM Management Libraries.
These repositories can be cloned by calling the **clone-repos.sh** or
**clone-repos.bat** script as appropriate. These scripts clone the various
repositories into the same folder as your **xnat-dicom** repository.

```bash
$ cd xnat-dicom
$ ./clone-repos.sh
# Here you'll see the output of the clone operation.
$ ls ..
dicom-image-utils/ dicom-xnat/        ecat/              ecat4xnat/         extattr/           prearc-importer/   session-builders/  xnat-dicom/
```

> **Note:** You will need to have a BitBucket account with a properly 
 configured SSH key for the **clone-repos.sh** script to work properly.
 If you don't have an account on BitBucket, you can [create one for free]
 (https://bitbucket.org/account/signup/). Once you have your account set up,
 [follow the instructions on BitBucket](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html)
 to get an SSH key set up and working properly. You can also modify the
 URLs to use https, which allows you to clone the repositories anonymously.
 Just change **git@bitbucket.org:** to **https://bitbucket.org/**. For example,
 the command to clone the **dicom-xnat** repository would change from this:

 ```bash
 $ git clone git@bitbucket.org:xnatdcm/dicom-xnat.git
 ```

 To this:

 ```bash
 $ git clone https://bitbucket.org/xnatdcm/dicom-xnat.git
 ```

Building
--------

Once you've cloned all of the repositories, you can build the libraries
using Maven (this requires at _least_ Maven 3.2.1, since the build uses
features that were added as of that version) from the **xnat-dicom**
folder. For example, to build and install the artifacts locally, you can
run:

```bash
$ cd xnat-dicom
$ mvn clean install
```

This will build all of the libraries in order of least to most 
dependency on the other libraries.

